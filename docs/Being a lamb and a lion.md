# Being a lamb and a lion

He kneeled down and cried with a loud voice, “Lord, don’t hold this sin against them!” When he had said this, he fell asleep. - Acts‬ 7‬:60‬ (WEBUS)‬‬

It is truly miraculous that the early church survived in the weeks following Jesus' ascension. In Jesus' lifetime, many people benefited from his teachings and miracles, but after his crucifixion and ascension, most disciples disappeared, except for 120. As soon as they were filled with the Holy Spirit, their circumstances dramatically changed. As a result of this divine intervention, they were transformed into a formidable force capable of changing the world. Empowered by God's presence, they displayed His power through words and miracles. As a result, thousands joined the early church daily, contributing to its growth and success.

Exponential growth brought ugly problems. There was dissatisfaction. There were murmurs. There were complaints. It could have derailed the church's journey. 

Thankfully the apostles didn't think they should receive all the attention. They probably remembered Jethro's advice to Moses. While leading the Israelites through the wilderness, Moses listened to every dispute the Israelites had and delivered them justice from morning till evening. His father-in-law, Jethro correctly observed that, "The thing that you do is not good. You will surely wear away, both you, and this people that is with you" (Exodus 18:17-18 WEBUS). Then he advised Moses to appoint capable men to judge, "you shall provide out of all the people able men which fear God: men of truth, hating unjust gain; and place such over them, to be rulers of thousands, rulers of hundreds, rulers of fifties, and rulers of tens.” ‭‭Exodus‬ ‭18‬:‭21‬ ‭(WEBUS)‬‬

The apostles established similar guidelines for selecting administrators. “Therefore, select from among you, brothers, seven men of good report, full of the Holy Spirit and of wisdom, whom we may appoint over this business.” - ‭‭Acts‬ ‭6‬:‭3‬ ‭(WEBUS)‬‬. By establishing this institution, the apostles probably established the first democratically established institution. The crowd chose Stephan and others to administer the table according to the apostles' guidelines. The apostles continued to pray and preach.

## Start serving

Stephan had an excellent reputation among people and was filled with the Holy Spirit. Later on, we see how well he understood the history and articulated his opinion. During his speech, he was able to stir up the spirits of his audience. He was an excellent public speaker. However, he did not request a position as a preacher.

The Bible says he was filled with the Holy Spirit. Ananias, in Acts 5, coveted the power of the Holy Spirit thinking it is some kind of magic. But the Lord gifted Stephan with the power of the Holy Spirit. He could have advertised himself as a miracle worker and attracted thousands of people towards him. He didn’t do that. 

He agreed to serve at the table. He did not consider doing menial tasks to be beneath his dignity. Stephan humbled himself, as Jesus instructed, instead of seeking positions of power. According to Matthew 23:11, "he who is greatest among you should be your servant." Immediately after the last supper, Jesus girded himself with a towel and washed disciples' feet and said, "For I have given you an example, that you should also do as I have done to you." John 13:15 (WEBUS)

Stephan followed Jesus' commandments and took care of widows even though he qualified for more. Sometimes, we might feel that we have all the qualifications to do higher status jobs and those in authority are not giving us the opportunities to prove our talents. Instead, you might feel the tasks given to you are simple and below your capabilities. As Jesus said in Matthew, when you are faithful in small things, greater things will come along your way. That is exactly what happened to Stephan. He was elected to serve at tables. But he was led to testify for Jesus before a large crowd. And we remember him even after two thousand years. 

## Be a lamp and a lion

John introduced Jesus as the Lamb of God who would take away the sins of the world, while in Revelation, the other John, saw him as the Lion of Judah. This duality was evident throughout Jesus' life; he welcomed sinners like prostitutes and tax collectors with compassion, yet spoke harsh words to religious leaders such as Pharisees and Sadducees. He commanded his disciples to be both harmless like doves and wise like serpents when spreading his teachings.

This dual nature requires wisdom from believers, which is why Proverbs dedicates itself to this subject and wisdom is considered one of the gifts of the Holy Spirit. Stephen exemplified this duality by humbly serving widows with gentleness but standing firm against religious authorities with courage and conviction.

Today's churches need divine wisdom to embody this dual nature. Often, we are too harsh on vulnerable members while failing to stand up against authority figures as required by our faith. Following Jesus' example means caring for those in need – widows, orphans, and others – while also holding authorities accountable for their actions.

Christians must strive for a balance between being gentle lambs towards those who are suffering while displaying lion-like courage when confronting injustice perpetrated by authoritative figures in society.

## When a grain of wheat dies, it bears much fruit

In John 12, Jesus states, "Verily, verily, I say unto you, unless a grain of wheat falls to the ground and dies, it cannot bear much fruit." This passage speaks to the story of Stephen: the first appointed member responsible for taking care of his church's vulnerable members. He was tragically stoned to death by an angry mob before his work could truly begin – seemingly marking an abrupt end for both him and his mission.

However, this was not the end; instead, like a fallen grain of wheat that bears fruit upon its demise, he too became a seed for much larger harvest. As he faced execution similar to Jesus himself - Stephen prayed for forgiveness on behalf of those who were killing him. Among them stood Saul – a man who not only approved but also participated in this brutal act.

Witnessing Stephen's remarkable final prayer must have affected Saul deeply as he later encountered Jesus during a journey to Damascus. While en route to Damascus, Saul encountered a bright light so intense that it brought him down to his knees—a vision strikingly similar to how Stephen fell under stones thrown by an unyielding mob. Struck down by divine light and humbled into submission - Saul asked what he should do next? From that moment on - everything changed.

Despite the turmoil and violence against followers like Stephen, Jesus' words held true: "Heaven and earth might pass away but my words will come to pass." 

The very same Saul who had once been instrumental in ending Stephen's life would go on to become one of Jesus' most devoted disciples. He authored half the New Testament while establishing churches throughout Jerusalem, Rome, Greece – even aspiring to reach Spain in spreading the gospel message further than ever before.

Though cut short unexpectedly early on -Stephen’s sacrifice proved powerful enough so that when one grain fell another might rise up carrying forth their shared purpose into every corner imaginable at their time period within history itself thus bearing much more abundant fruit than initially anticipated because true faith can never be extinguished entirely no matter how bleak circumstances may seem.

***

back to [[index]]