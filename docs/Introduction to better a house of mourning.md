# Introduction to better a house of mourning

So teach us to count our days, that we may gain a heart of wisdom. - ‭‭Psalms‬ ‭90‬:‭12‬ ‭(WEBUS)‬‬

A French newspaper published a scathing obituary of a merchant. He read it, changed his life, and left a lasting legacy. That merchant was Alfred Nobel.

The newspaper mistook his brother's death as his and [wrote](https://en.wikipedia.org/wiki/Alfred_Nobel#Nobel_Prize):

> The merchant of death is dead
> Dr. Alfred Nobel, who became rich by finding ways to kill more people faster than ever before, died yesterday.

He was stung by the "merchant of death" epithet. He set aside large part of his wealth to establish five annual prizes bearing his name. **He was fortunate to read his obituary**.

Steve Jobs introduced a series of revolutionary products after he was diagnosed with cancer. His products brought fortunes to Apple. But his thinking had a ripple effect far beyond the borders of Apple. **When he numbered his days, he gained a heart of wisdom**.

He said to the [Standford students](https://news.stanford.edu/2005/06/14/jobs-061505/): 

> Remembering that I'll be dead soon is the most important tool I've ever encountered to help me make the big choices in life.

I think of my death often. I've been thinking about my death ever since I read Steven Covey's phenomenal book. In "[Seven habits](https://amzn.to/33X6mbq)," he encouraged his readers to **visualize their funeral**. He set the scene vividly:

> In your mind's eye, see yourself going to the funeral parlor, parking the car, and getting out. As you walk inside the building, you notice the flowers, the soft organ music. You see faces of friends and family you pass along the way. You feel the shared sorrow of losing, the joy of having known, that radiates from the hearts of the people there.

Here is your funeral. Your family, friends, and colleagues have come to honor you.

> Now think deeply. What would you like each of these speakers to say about you and your life? What kind of husband, wife, father, or mother would you like their words to reflect? What kind of son or daughter or cousin? What kind of friend? What kind of working associate? What character would you like them to have seen in you? What contributions? What achievements?

When I read these words for the first time as a twenty-seven-year-old, they felt creepy, eerie, and futile. I ignored the exercise. Even though I ignored it, the eulogy exercise clang to my unconscious mind for years after I finished the book.

A month before my marriage, the exercise seemed relevant. I sat in a quiet place and wrote what I would want my future wife to say as her obituary.

> We built our home together, not with gold, silver, and dollars but with laughter and love. He never compelled me to do anything which I didn't want but always encouraged me to explore worlds beyond my immediate comprehension. When I felt tired by such exploration, he stood as a pillar of strength and encouragement on which I can lean to rest and refresh. I can't say if he was a husband or friend or father or son because he was all that to me. On the day of our engagement, I asked permission for only one thing. He ensured I was able to do that until his last laughter. He always said, "if I lived a life worth honoring, don't cry that I died, but celebrate that I lived a life of worth." So, though my heart is heavy, eyes are full, and I am lonely for the first time in decades, I am going to celebrate the life we lived.

When my wife was admitted for the delivery of our first child, which happened to be a son, I augmented the "obituary" with his words.

_She was in the hospital for 15 days due to pregnancy complications. So I wrote this with ample time in hand but mostly anxiety in heart_

> Today, I lost a cistern from which I drank wisdom, discipline, and courage. Appa led by example with two Bible verses. The first one was from the book of Proverbs. It says, "The horse is prepared for the battle, but the victory comes from the Lord." He prepared us for life. We read diverse books, kept ourselves fit, and pursued exciting hobbies. We ate with kids in slums as well as kids in bungalows. He taught us never to feel haughty in slums nor inferior in bungalows. He encouraged us to have God-sized dreams but trained us to accept situations as God-sent.

> The other verse was from Saint Paul. Dad was a Paul-fanatic. Not only he devoured everything about Paul, but he also tried to live as much as possible like Paul. We had a daily study of Paul's letters. Paul's letters shaped our worldview. In his letters to Romans, Paul writes, "let not the patterns of the world entangled you. Rather transform yourself by the constant renewal of the mind." Daddy insisted that we never follow a line of thought or a path because that's the way of the masses. He trained us to think for ourselves and chose a path of our selection. When we found ourselves in predicaments, he would ask us if biases constrain us.

> He led by example, much like Paul. If he wanted us to wake up at 6, he was up at 5; if he wanted us to read for 10 min, he read for an hour; if he wanted us to respect amma, he treated her like a queen, true to her name. From the time we were kids, he treated us as equals. He didn't do anything at home without discussing it with us. He made us feel as if we advised him. Whenever we faced a loss, he comforted us, saying not to weep because we lost but to rejoice because we had the privilege of experiencing it. Today, I am lost if I should weep or rejoice. I looked into the cistern that gave me answers. But it is dry.
> 

In addition, I wrote what my colleagues wanted to say, which became an operating guideline for my career. The guidelines read:  
  
- **Life is fun if you don’t compare**. I am happy if I am better than I was. I don’t compare myself to others and feel neither elated nor dejected.  
- **Life is a non-zero sum game**. I don’t have to lose for others to win. We can find a deal that works for both parties in the game.  
- I like to **work with people I trust**. That way, I can channelise my focus on what I’m creating rather than worrying if I will get my part of the deal. No amount of contractual clauses can assure confidence, if you don’t trust other parties in the game.  
- Because I work with people I trust, **I seldom negotiate**. If I get a fair deal, I accept. When I negotiate hard I get more, but I become less.  
- It’s **no humility to live a lesser life** than the one you are capable of living. In fact it’s a crime to live such a life.

Though it took many days, I took the exercise seriously, thought deeply and wrote each word sincerely. I wasn't sure what impact it would have on me. Will mere words change my life? Is it all an illusion? As I write this book, nearly fifteen years have passed since I wrote these words. To say that my life was transformed by these words would be an understatement. 

Meditating on death and reflecting on how to live life has given me clarity about my life's purpose, which I consider the core of my transformation. This insight has guided me in choosing which opportunities to pursue and which ones to decline, significantly reducing anxiety and stress related to building my career. Over the past 15 years, I have had a meaningful career journey. I served as a consultant to the Indian government for six years before starting a startup that ultimately failed but provided invaluable learning experiences. Gradually, I built a successful technology career as Chief Technology Officer of an IT services company. Since career forms a significant part of one's life and identity, this alone is a worthwhile benefit of the exercise.

Having a deep understanding of death helped me build a loving family life. We have only 12 summers with our children before time flies. Given the brevity of this period, I chose to work three days per week and devote the rest of my time to my growing boys. Every day we shared a meal together, read stories, and played games. This routine led me to homeschool them from the age of eleven. I have heavily invested in their lives and hope these efforts will bear fruit in the future. I have developed a similar bonding ritual with my wife. We pray together every morning and have lunch together every Sunday. We also pray almost every day as a family.

On any given day, I am content with what I have and what I've accomplished. Despite this, like St. Paul, I remain ambitious about what I want to be and do. According to St. Paul, godliness with contentment is good. He says, “I have learned to live in abundance and in need.” But this is the man who single-handedly took Christianity to all known parts of the world of his time. He writes about his ambition to spread the gospel even further. Much like Paul, I would like to say at the end of my days that I've run my race and kept my faith.

As the Psalmist sang in Psalms 122, this all seemed like a dream. Each time I knelt to pray, I wondered if all of these things were really happening to me. Could this have all resulted from meditating on death?

In the mean time, I got to attend the memorial service for my uncle. As you could imagine, I was reminded of Ecclesiastes 7:2.

> It is better to go to the house of mourning than to go to the house of feasting; for that is the end of all men, and the living should take this to heart.

King Solomon repeats again.

> Better is the end of a thing than its beginning.

As I stood there in the cemetery, I remembered the impact my uncle had on our lives. He got a job for my mother and gave us a head-start on prosperity. Not only did he put us on that journey, he visited us every year with his family. He stayed in our small one-bed room house and ate rice porridge that we offered without any qualms. He preached humility and kindness in action.

As I pondered his life and all he meant to me, an idea came to me. What if I could attend the funerals of the characters mentioned in the Bible? What would I consider? What lessons would I learn from their lives? I could observe their relationships with God, understand their struggles and triumphs, and gain insight into their lives. I could use these insights to help me grow in my own faith journey. Most importantly, I could appreciate the legacy they left behind and gain a better understanding of God's plan for each of us.

I began meditating on the deaths mentioned in the Bible. The meditation only got deeper and deeper as I meditated, just like any other topic. I felt somber and euphoric at the same time. Through meditation, I developed a deeper understanding of the Biblical perspective on life and a profound connection with those I meditated with.

This book focuses on those whose deaths are mentioned in the Bible. So Paul and Peter aren't mentioned here, not because they didn't live lives worth contemplating. Although Jesus' death is recorded in the Bible, I am not including it here since I think two pages won't cover everything meaningful to learn from life and death of Jesus.

These pages are my attempt to express my insights in a sufficient manner. So you can find the same Biblical perspective on life that changed my life as well.

***
back to [[index]] / read next chapter - [[Being a lamb and a lion]]